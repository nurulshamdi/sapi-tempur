FROM node:14-alpine AS builder

WORKDIR /app
COPY package.json ./
RUN npm install --production
COPY . .

FROM node:14-alpine

WORKDIR /app
COPY --from=builder /app .
CMD ["node", "backend-nodejs/server.js"]

